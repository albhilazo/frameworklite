<?php

class Model extends Db
{
	protected $db;

	function __construct()
	{
		// Connecction to database
		$this->init();
		return Db::getInstance();
	}

	protected function getClass( $class )
	{
		return Factory::getClass( $class );
	}

	/**
	 * Return the values stored in the config file. If you request a value it returns the specific value.
	 * In the other cases it returns the hold config.
	 *
	 * @param string $name
	 * @param string $value
	 * @return object
	 */
	protected function getConfig( $name, $value = null )
	{
		$config 	= Configure::getInstance( $name );

		if ( $value === null )
		{
			return $config->getAll();
		}

		return $config->get( $value );
	}

}
?>