<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="title" content="" />
	<meta name="robots" content="all" />
	<meta name="expires" content="never" />
	<meta name="distribution" content="world" />		
	<title>Framework Lite</title>
	<link rel="stylesheet" href="{$url.global}/css/style.css">
</head>
<body>
	<div class="main_header">
		<header>
			<div class="site-logo" name="top"><a href="{$url.global}">Frameworklite</a></div>
		</header>
	</div>
	<div id="wrapper">